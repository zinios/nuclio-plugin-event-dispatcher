<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\event\dispatcher
{
	require_once(__DIR__.'/types.hh');
	
	use nuclio\core\
	{
		plugin\Plugin,
		ClassManager
	};
	use nuclio\plugin\
	{
		event\dispatcher\exception\DispatcherException,
		config\Config,
		http\uri\URI
	};
	
	/**
	 * The Dispatcher is responsible for receiving actions and
	 * dispatching those actions to handlers who have registered
	 * themselves to receive action.
	 * 
	 * An action is simply an event which is fired off by something,
	 * received by the Dispatcher and passed along to other entities in
	 * the system which have registered listeners against that action.
	 * 
	 * An action may contain a payload which is passed to listeners.
	 */
	<<
	singleton,
	provides('event::dispatcher',0)
	>>
	class Dispatcher extends Plugin
	{
		/**
		 * Contains all reigstered callbacks against actions.
		 * 
		 * @private
		 * @var Map<string,CallbackMap>
		 */
		private Registry $registry			=Map{};
		private WaitingQueue $waitingQueue	=Vector{};
		public ConfigCollection $config		=Map{};
		public URI $URI;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Dispatcher
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct()
		{
			parent::__construct();
			$this->URI=URI::getInstance();
			$this->loadActions();
		}
		
		public function bindConfig(Config $config):this
		{
			$this->config=$config;
			return $this;
		}
		
		public function bindURI(URI $URI):this
		{
			$this->URI=$URI;
			return $this;
		}
		
		public function loadActions():void
		{
			$mapFile=realpath(ROOT_DIR)._DS_.'.nuclio'._DS_.'actions.map';
			if (file_exists($mapFile))
			{
				if (!is_readable($mapFile))
				{
					throw new DispatcherException(sprintf('Actions cache file "%s" is not readable.',$mapFile));
				}
				$actions=unserialize(file_get_contents($mapFile));
				if (!is_null($actions))
				{
					foreach ($actions as $actionName=>$callback)
					{
						$this->register($actionName,$callback);
					}
				}
			}
			return $this;
		}
		
		/**
		 * Registers a callback against an action.
		 * 
		 * @param  Action   $action   The action to register against.
		 * @param  Callback $callback The callback method to be registered against the action.
		 * @return this
		 */
		public function register(Action $action,mixed $callback):this
		{
			if (!$this->registry->containsKey($action))
			{
				$this->registry->set($action,$callback);
			}
			else
			{
				$parts		=explode('\\',(string)$callback->get('class'));
				$namespace	=end($parts);
				//TODO: Track who registered the action for debugging purposes.
				$message=<<<MESSAGE
An action with the reference "%s" has already been registered.<br>
Registering multiple callbacks against an action is not allowed.<br>
One of the actions must be changed.<br>
I Suggest you rename or give it a namespace (ie. %s.%s ).
MESSAGE;
				throw new DispatcherException($message,$action,$namespace,$action);
			}
			return $this;
		}
		
		/**
		 * Dispatches an event with a payload. Any callbacks registered against
		 * the action will be executed.
		 * 
		 * @param  Action  $action  The action to be dispatched.
		 * @param  Payload $payload A payload to which is passed along with the action.
		 * @return this
		 */
		public function dispatch(Action $action,?Payload $payload=null):?bool
		{
			if (!$this->registry->count())
			{
				$message=<<<MSG
Actions have not been mapped<br>
You can map them with the nuclio command line tool.<br>
Use the "actions::map" command.<br>
Or re-run the "setup" command.
MSG;
			}
			$return=true;
			if ($this->registry->containsKey($action))
			{
				$callback=$this->registry->get($action);
				if (!is_null($callback))
				{
					try
					{
						if ($callback instanceof Map)
						{
							$class	=$callback->get('class');
							$method	=$callback->get('method');
							if (!is_null($class) && !is_null($method))
							{
								$controllerInstance=ClassManager::getClassInstance
								(
									$class,
									$this->getURI(),
									$this->getConfig()->get('controller')
								);
								if (method_exists($controllerInstance,$method))
								{
									$response=call_user_func_array([$controllerInstance,$method],[$payload]);
									if ($response->isWaiting())
									{
										$this->waitFor($response->getWaitingFor(),$response->getCallback());
									}
									else if ($response->isFailure())
									{
										$return=false;
									}
								}
								else
								{
									$message=<<<MSG
Invalid action!<br>
The action "%s" does not correctly map to an existing method.<br>
Please re-run the "actions::map" command with the nuclio command line tool.<br>
Or re-run the "setup" command.
MSG;
									throw new DispatcherException(sprintf($message,$controllerAction,$controller));
								}
							}
						}
						else
						{
							//hmmm. This is not valid Hack :D
							//TODO: Fix this.
							$response=$callback($action,$payload);
							if ($response->isWaiting())
							{
								$this->waitFor($response->getWaitingFor(),$response->getCallback());
								$return=null;
							}
							else if ($response->isFailure())
							{
								$return=false;
							}
						}
					}
					catch(\Exception $exception)
					{
						$newPayload=Map
						{
							'message'		=>$exception->getMessage(),
							'originalAction'=>Map
							{
								'action'	=>$action,
								'payload'	=>$payload
							}
						};
						try
						{
							//We don't allow waiting on for errors which have been dispatched.
							$this->dispatch('DISPATCHER.ERROR',$newPayload);
						}
						catch(\Exception $exception)
						{
							throw new DispatcherException('Unrecoverable exception. Exception Action threw another exception.',[$exception,$newPayload]);
						}
					}
				}
			}
			/*
				Actions that are waiting for other actions to complete
				are executed at this stage.
				This assures that the action that has just been dispatched
				has been given a chance to complete (or wait)
			 */
			//Clone it first as we may need to modify it.
			$queue=new Vector($this->waitingQueue);
			foreach ($queue as $i=>$waitingDef)
			{
				$waitingDef		=new Map($waitingDef);
				$waitList		=$waitingDef->get('waitList');
				$completeList	=$waitingDef->get('completeList');
				if (!is_null($waitList) && !is_null($completeList))
				{
					if ($waitList->linearSearch($action)!==-1
					&& !$completeList->get($action))
					{
						$completeList->set($action,true);
					}
					$waitingDef->set('completeList',$completeList);
					
					$waitingCompleted=true;
					foreach ($completeList as $action=>$complete)
					{
						if (!$complete)
						{
							$waitingCompleted=false;
							break;
						}
					}
					if ($waitingCompleted)
					{
						$callback=$waitingDef->get('callback');
						if (!is_null($callback))
						{
							$this->waitingQueue->removeKey($i);
							$response=$callback();
							if ($response->isWaiting())
							{
								$this->waitFor($response->getWaitingFor(),$response->getCallback());
								$return=null;
							}
							else if ($response->isFailure())
							{
								$return=false;
							}
						}
					}
				}
			}
			return $return;
		}
		
		public function waitFor(WaitList $waitList,WaitingCallback $callback)
		{
			$completeList=Map{};
			foreach ($waitList as $waitItem)
			{
				$completeList->set($waitItem,false);
			}
			$this->waitingQueue->add
			(
				shape
				(
					'waitList'		=>$waitList,
					'completeList'	=>$completeList,
					'callback'		=>$callback
				)
			);
		}
		
		public function getConfig():Config
		{
			return $this->config;
		}
		
		public function getURI():URI
		{
			return $this->URI;
		}
	}
}
