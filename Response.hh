<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\event\dispatcher
{
	use nuclio\plugin\dispatcher\exception\DispatcherException;
	
	class Response
	{
		private mixed $response;
		private ?WaitingCallback $callback;
		
		public function __construct(mixed $response,?WaitingCallback $callback=null)
		{
			$this->response=$response;
			$this->callback=$callback;
			if (!$this->response instanceof Vector
			&& !is_bool($this->response))
			{
				throw new DispatcherException('Response was invalid. Must be either a Vector of response actions to wait for or a boolean.');
			}
		}
		
		public function getResponseType():string
		{
			if ($this->response instanceof Vector)
			{
				return 'WaitList';
			}
			else
			{
				return 'bool';
			}
		}
		
		public function isWaiting():bool
		{
			return $this->response instanceof Vector;
		}
		
		public function isSuccessful():bool
		{
			return (is_bool($this->response) && $this->response===true);
		}
		
		public function isFailure():bool
		{
			return (is_bool($this->response) && $this->response===false);
		}
		
		public function getWaitingFor():WaitList
		{
			return $this->response;
		}
		
		public function getCallback():WaitingCallback
		{
			return $this->callback;
		}
		
		public function executeCallback():Response
		{
			$callback=$this->callback;
			return $callback();
		}
	}
}
