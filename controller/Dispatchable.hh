<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\event\dispatcher\controller
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\event\dispatcher\
	{
		Dispatcher,
		Action,
		WaitList,
		Payload,
		Callback,
		WaitingCallback,
		Response
	};
	
	trait Dispatchable
	{
		require extends Controller;
		
		public function dispatch(Action $action,?Payload $payload=null):bool
		{
			return Dispatcher::getInstance()->dispatch($action,$payload);
		}
		
		public function respond(mixed $response,?WaitingCallback $callback=null):Response
		{
			return new Response($response,$callback);
		}
		
		public function waitFor(WaitList $waitList,?WaitingCallback $callback=null):Response
		{
			return $this->respond($waitList,$callback);
		}
	}
}
