<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\event\dispatcher
{
	type Action				=string;
	type Payload			=Map<string,mixed>;
	type Callback			=(function(Action,Payload):bool);
	type WaitingCallback	=(function():Response);
	type CallbackMap		=Map<string,string>;
	type Registry			=Map<string,CallbackMap>;
	type WaitList			=Vector<Action>;
	type WaitCompleteList	=Map<string,bool>;
	type WaitShape			=shape
							(
								'waitList'=>WaitList,
								'doneList'=>WaitCompleteList,
								'callback'=>WaitingCallback
							);
	type WaitingQueue		=Vector<WaitShape>;
}
